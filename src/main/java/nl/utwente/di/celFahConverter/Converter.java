package nl.utwente.di.celFahConverter;

public class Converter {

    public double convertCelToFah(double input){
        return (input*1.8)+32;
    }
}
