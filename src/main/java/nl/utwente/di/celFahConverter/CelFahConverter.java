package nl.utwente.di.celFahConverter;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/***
 * serverlet that converts celsius to fahrenheit temperature
 */
public class CelFahConverter extends HttpServlet{

    Converter converter;

    public void init() throws ServletException {
        converter = new Converter();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!DOCTYPE HTML>\n";
        String title = "Celsius to Fahrenheit Converter";
        out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                "</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +
                "  <P>Celsius: " +
                request.getParameter("celsius") + "\n" +
                "  <P>Fahrenheit: " +
                Double.toString(converter.convertCelToFah(Integer.parseInt(request.getParameter("celsius")))) +
                "</BODY></HTML>");
    }
}
